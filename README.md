# Getting Started on Web Store Products

### Considerations
#### Different Structures used
* Products

For Products, I consider using a Set (Hashset) because I thought that it could be better to access by an id. Finally, I never used it, but I thought that it was right to sort them later.
  
* Sizes

For Sizes, I consider using a list because I can filter it and I never gonna find a size explicitly.
* Stocks

For Stocks, I consider using a Map because I'm going to go to find one element by id, and I only need the value for this key.

#### Complexity
The algorithm has the complexity n*m*z where n it's the number of products, m the number of sizes and z the number of stock items. 

### Other considerations
At the beginning as you can see in the commits, I start the project as a Springboot API Service.
In the second step, I though that it was only a java program, and I change everything.
At last, I read again, and I transform it as a Springboot java program, and I've done a big refactor to use spring injections facilities. 
This is my fault.


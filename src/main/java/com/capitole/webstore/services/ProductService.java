package com.capitole.webstore.services;

import com.capitole.webstore.entities.Product;

import java.io.IOException;
import java.util.List;


public interface ProductService {
	String getVisibleProducts() throws IOException;

	List<Product> getProducts() throws IOException;
}

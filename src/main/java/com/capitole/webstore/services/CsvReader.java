package com.capitole.webstore.services;

import java.io.IOException;
import java.util.List;


public interface CsvReader<T> {
	List<T> readFile() throws IOException;
}

package com.capitole.webstore.services;

import com.capitole.webstore.entities.Size;

import java.io.IOException;
import java.util.List;


public interface SizeService {
	List<Size> getSizes() throws IOException;
}

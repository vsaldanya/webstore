package com.capitole.webstore.services.impl;

import com.capitole.webstore.entities.Product;
import com.capitole.webstore.entities.Size;
import com.capitole.webstore.entities.Stock;
import com.capitole.webstore.services.CsvReader;
import com.capitole.webstore.services.ProductService;
import com.capitole.webstore.services.SizeService;
import com.capitole.webstore.services.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

	private final CsvReader<Product>    csvReader;
	private final SizeService  sizeService;
	private final StockService stockService;

	public ProductServiceImpl(@Qualifier("csvReaderProducts") CsvReader<Product> csvReader, SizeService sizeService, StockService stockService) {
		this.csvReader = csvReader;
		this.sizeService = sizeService;
		this.stockService = stockService;
	}

	@Override
	public String getVisibleProducts() throws IOException {
		try {
			HashMap<Integer, Integer> result = new HashMap<>();
			Set<Product> products = new HashSet<>(getProducts());
			List<Size> sizes = sizeService.getSizes();
			Map<Integer, Integer> stocks = stockService.getStocks().stream().collect(Collectors.toMap(Stock::getSizeId, Stock::getQuantity));

			products.forEach(product -> {
				if (checkProduct(product, sizes, stocks)) {
					result.put(product.getId(), product.getSequence());
				}
			});
			StringBuilder sb = new StringBuilder();
			Stream<Map.Entry<Integer, Integer>> sorted = result.entrySet().stream().sorted(Map.Entry.comparingByValue());
			sorted.forEach(p -> sb.append(p.getKey()).append(","));
			log.info("SUCCESS!");
			return sb.toString();
		} catch (IOException e) {
			log.error("ERROR Reading file: %s", e);
			throw e;
		} catch (Exception e) {
			log.error("GENERAL ERROR: %s", e);
			throw e;
		}
	}

	@Override
	public List<Product> getProducts() throws IOException {
		return csvReader.readFile();
	}

	private boolean checkProduct(Product product, List<Size> sizes, Map<Integer, Integer> stocks) {
		boolean hasToAppear;
		AtomicBoolean isSpecial = new AtomicBoolean(false);
		AtomicBoolean hasNormalStock = new AtomicBoolean(false);
		AtomicBoolean hasSpecialStock = new AtomicBoolean(false);

		List<Size> listOfSizes = sizes.stream().filter(x -> x.getProductId() == product.getId()).collect(Collectors.toList());

		listOfSizes.forEach(size -> {
			Integer stockValue = stocks.get(size.getId());
			int stock = stockValue != null ? stockValue : 0;

			if (size.isSpecial()) {
				isSpecial.set(true);
				if (stock > 0 || size.isBackSoon()) {
					hasSpecialStock.set(true);
				}
			} else if (stock > 0 || size.isBackSoon()) {
				hasNormalStock.set(true);
			}
		});

		hasToAppear = (isSpecial.get() && hasNormalStock.get() && hasSpecialStock.get()) || (!isSpecial.get() && hasNormalStock.get());
		return hasToAppear;
	}
}

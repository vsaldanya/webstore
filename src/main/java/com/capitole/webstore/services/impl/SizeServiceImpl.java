package com.capitole.webstore.services.impl;

import com.capitole.webstore.entities.Size;
import com.capitole.webstore.services.CsvReader;
import com.capitole.webstore.services.SizeService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class SizeServiceImpl implements SizeService {

	private final CsvReader<Size> csvReader;

	public SizeServiceImpl(@Qualifier("csvReaderSizes") CsvReader<Size> csvReader) {
		this.csvReader = csvReader;
	}

	@Override public List<Size> getSizes() throws IOException {
		return csvReader.readFile();
	}
}

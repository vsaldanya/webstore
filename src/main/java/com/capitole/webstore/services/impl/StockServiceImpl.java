package com.capitole.webstore.services.impl;

import com.capitole.webstore.entities.Stock;
import com.capitole.webstore.services.CsvReader;
import com.capitole.webstore.services.StockService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;


@Service
public class StockServiceImpl implements StockService {


	private final CsvReader<Stock> csvReader;

	public StockServiceImpl(@Qualifier("csvReaderStocks") CsvReader<Stock> csvReader) {
		this.csvReader = csvReader;
	}

	@Override public List<Stock> getStocks() throws IOException {
		return csvReader.readFile();
	}
}

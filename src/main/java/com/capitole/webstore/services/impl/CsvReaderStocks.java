package com.capitole.webstore.services.impl;

import com.capitole.webstore.entities.Stock;
import com.capitole.webstore.services.CsvReader;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;


@Service
public class CsvReaderStocks implements CsvReader<Stock> {
	@Override
	public List<Stock> readFile() {
		List<Stock> stocks;

		stocks = new CsvToBeanBuilder<Stock>(new BufferedReader(new InputStreamReader(Objects.requireNonNull(this.getClass().getResourceAsStream("/stock.csv")))))
				.withType(Stock.class)
				.build()
				.parse();


		return stocks;
	}
}

package com.capitole.webstore.services.impl;

import com.capitole.webstore.entities.Size;
import com.capitole.webstore.services.CsvReader;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;


@Service
public class CsvReaderSizes implements CsvReader<Size> {
	@Override
	public List<Size> readFile() {
		List<Size> sizes;


		sizes = new CsvToBeanBuilder<Size>(new BufferedReader(new InputStreamReader(Objects.requireNonNull(this.getClass().getResourceAsStream("/size.csv")))))
				.withIgnoreQuotations(true)
				.withType(Size.class)
				.build()
				.parse();


		return sizes;
	}
}

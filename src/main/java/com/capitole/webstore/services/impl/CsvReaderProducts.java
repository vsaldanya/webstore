package com.capitole.webstore.services.impl;

import com.capitole.webstore.entities.Product;
import com.capitole.webstore.services.CsvReader;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;


@Service
public class CsvReaderProducts implements CsvReader<Product> {
	@Override
	public List<Product> readFile() {
		List<Product> products;
		products = new CsvToBeanBuilder<Product>(new BufferedReader(new InputStreamReader(Objects.requireNonNull(this.getClass().getResourceAsStream("/product.csv")))))
				.withType(Product.class)
				.build()
				.parse();

		return products;
	}
}

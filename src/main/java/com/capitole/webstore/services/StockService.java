package com.capitole.webstore.services;

import com.capitole.webstore.entities.Stock;

import java.io.IOException;
import java.util.List;


public interface StockService {
	List<Stock> getStocks() throws IOException;
}

package com.capitole.webstore.entities;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Stock {
	@CsvBindByPosition(position = 0)
	private int sizeId;
	@CsvBindByPosition(position = 1)
	private int quantity;

}

package com.capitole.webstore;

import com.capitole.webstore.services.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@Slf4j
public class StoreApplication implements CommandLineRunner {

	private final ProductService productService;

	public StoreApplication(ProductService productService) {
		this.productService = productService;
	}

	public static void main(String[] args) {
		SpringApplication.run(StoreApplication.class, args);
	}

	@Override
	public void run(String... args) {
		try {
			log.info(productService.getVisibleProducts());
		} catch (Exception e) {
			log.error("ERROR, %s", e);
		}
	}

}
